#define NDEVICEMFI_OUTLET_NMFIOUTLETDEVICESERVICE_INTERNE
#include "../../include/NDeviceMFI.h"

// ------------------------------------------------
// enum NDeviceMFI::Outlet::NMFIOutletDeviceService
// ------------------------------------------------

/**
 * Find service
 *
 * @param name
 * 		The service name
 *
 * @return the service type or NMFI_OUTLET_SERVICES
 */
NMFIOutletDeviceService NDevice_Outlet_NMFIOutletDeviceService_FindService( const char *name )
{
	// Iterator
	NMFIOutletDeviceService serviceType;

	// Syntax count
	NU32 syntax = 0;

	// Look for
	for( ; syntax < NMFI_OUTLET_DEVICE_SERVICE_SYNTAX_COUNT; syntax++ )
		for( serviceType = (NMFIOutletDeviceService)0; serviceType < NMFI_OUTLET_DEVICE_SERVICES; serviceType++ )
			// Check
			if( NLib_Chaine_Comparer( name,
				NMFIOutletDeviceServiceName[ syntax ][ serviceType ],
				NTRUE,
				0 ) )
				// Found it!
				return serviceType;

	// Not found
	return NMFI_OUTLET_DEVICE_SERVICES;
}

/**
 * Is correct data type
 *
 * @param serviceType
 * 		The service type
 * @param parserDataType
 * 		The data type
 *
 * @return if this is the correct data type
 */
NBOOL NDevice_Outlet_NMFIOutletDeviceService_IsCorrectDataType( NMFIOutletDeviceService serviceType,
	NParserOutputType parserDataType )
{
	return (NBOOL)( NMFIOutletDeviceServiceType[ serviceType ] == parserDataType );
}
