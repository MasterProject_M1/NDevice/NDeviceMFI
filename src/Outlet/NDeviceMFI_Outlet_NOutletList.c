#include "../../include/NDeviceMFI.h"

// --------------------------------------
// struct NDeviceMFI::Outlet::NOutletList
// --------------------------------------

/**
 * Build outlet list
 *
 * @return the outlet list instance
 */
__ALLOC NOutletList *NDeviceMFI_Outlet_NOutletList_Build( void )
{
	// Output
	__OUTPUT NOutletList *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NOutletList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build list
	if( !( out->m_list = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NDeviceMFI_Outlet_NOutlet_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy outlet list instance
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletList_Destroy( NOutletList **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_list );

	// Free
	NFREE( (*this) );
}

/**
 * Update outlet list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The new incoming data
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceMFI_Outlet_NOutletList_Update( NOutletList *this,
	const NParserOutputList *parserOutputList )
{
	// Data
	struct NOutletParseData data;

	// Outlet
	NOutlet *outlet;

	// Cursor
	NU32 cursor = 0;

	// Check data count
	if( NParser_Output_NParserOutputList_GetEntryCount( parserOutputList ) <= 1 )
		// Nothing done, that's was a status modification request ({"status":"success"})
		return NFALSE;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_list );

	// Flush list
	while( NLib_Memoire_NListe_ObtenirNombre( this->m_list ) )
		NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_list,
			0 );

	// Read port data
	while( cursor < NParser_Output_NParserOutputList_GetEntryCount( parserOutputList )
		&& NDeviceMFI_Outlet_NOutletParseData_ReadPortData( parserOutputList,
			&cursor,
			&data ) )
	{
		// Build outlet
		if( !( outlet = NDeviceMFI_Outlet_NOutlet_Build( &data ) ) )
		{
			// Unlock
			NLib_Memoire_NListe_DesactiverProtection( this->m_list );

			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quit
			return NFALSE;
		}

		// Add outlet
		NLib_Memoire_NListe_Ajouter( this->m_list,
			outlet );
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_list );

	// OK
	return NTRUE;
}

/**
 * Get outlet index from name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The outlet name
 *
 * @return the outlet index or NERREUR
 */
__MUSTBEPROTECTED NU32 NDeviceMFI_Outlet_NOutletList_GetOutletIndexFromName( const NOutletList *this,
	const char *name )
{
	// Iterator
	__OUTPUT NU32 i = 0;

	// Outlet
	const NOutlet *outlet;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
		// Get outlet
		if( ( outlet = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i ) ) != NULL )
			// Check name
			if( NDeviceMFI_Outlet_NOutlet_GetName( outlet ) != NULL
				&& NLib_Chaine_Comparer( NDeviceMFI_Outlet_NOutlet_GetName( outlet ),
					name,
					NTRUE,
					0 ) )
				// OK
				return i;

	// Not found
	return NERREUR;
}

/**
 * Get outlet count
 *
 * @param this
 * 		This instance
 *
 * @return the outlet count
 */
__MUSTBEPROTECTED NU32 NDeviceMFI_Outlet_NOutletList_GetOutletCount( const NOutletList *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_list );
}

/**
 * Get outlet
 *
 * @param this
 * 		This instance
 * @param index
 * 		The outlet index
 *
 * @return the outlet
 */
__MUSTBEPROTECTED const NOutlet *NDeviceMFI_Outlet_NOutletList_GetOutlet( const NOutletList *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
		index );
}

/**
 * Process outlet operation
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param index
 * 		The outlet index
 * @param isEnabled
 * 		Is outlet enabled?
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceMFI_Outlet_NOutletList_ProcessOutletOperation( NOutletList *this,
	NClientHTTP *httpClient,
	NU32 index,
	NBOOL isEnabled )
{
	// Outlet
	NOutlet *outlet;

	// Check index
	if( index >= NLib_Memoire_NListe_ObtenirNombre( this->m_list ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Get outlet
	if( !( outlet = (NOutlet*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
		index ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Execute
	return NDeviceMFI_Outlet_NOutlet_ProcessOutletOperation( outlet,
		httpClient,
		isEnabled );
}

/**
 * Process outlet operation
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param name
 * 		The outlet name
 * @param isEnabled
 * 		Is outlet enabled?
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceMFI_Outlet_NOutletList_ProcessOutletOperation2( NOutletList *this,
	NClientHTTP *httpClient,
	const char *name,
	NBOOL isEnabled )
{
	// Index
	NU32 index;

	// Find index
	if( ( index = NDeviceMFI_Outlet_NOutletList_GetOutletIndexFromName( this,
		name ) ) == NERREUR )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Execute
	return NDeviceMFI_Outlet_NOutletList_ProcessOutletOperation( this,
		httpClient,
		index,
		isEnabled );
}

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletList_ActivateProtection( NOutletList *this )
{
	NLib_Memoire_NListe_ActiverProtection( this->m_list );
}

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletList_DeactivateProtection( NOutletList *this )
{
	NLib_Memoire_NListe_DesactiverProtection( this->m_list );
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param serviceType
 * 		The service type
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
__PRIVATE __MUSTBEPROTECTED NBOOL NDeviceMFI_Outlet_NOutletList_BuildParserOutputListInternal( NOutletList *this,
	__OUTPUT NParserOutputList *parserOutputList,
	NMFIOutletListService serviceType,
	const char *keyRoot )
{
	// Key
	char key[ 128 ];

	// Outlet
	const NOutlet *outlet;

	// Iterator
	NU32 i = 0;

	// Build key
	switch( serviceType )
	{
		case NMFI_OUTLET_LIST_SERVICE_OUTLET:
			break;

		default:
			// Build key
			snprintf( key,
				128,
				"%s%s%s",
				keyRoot,
				strlen( keyRoot ) > 0 ?
					"."
					: "",
				NDeviceMFI_Service_NMFIOutletListService_GetName( serviceType ) );
			break;
	}

	// Add data
	switch( serviceType )
	{
		case NMFI_OUTLET_LIST_SERVICE_COUNT:
			NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				NLib_Memoire_NListe_ObtenirNombre( this->m_list ) );
			break;
		case NMFI_OUTLET_LIST_SERVICE_OUTLET:
			// Iterate outlet
			for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
				// Get outlet
				if( ( outlet = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
					i ) ) != NULL )
				{
					// Build key
					snprintf( key,
						128,
						"%s%s%d",
						keyRoot,
						strlen( keyRoot ) > 0 ?
							"."
							: "",
						i );

					// Process
					NDeviceMFI_Outlet_NOutlet_BuildParserOutputList( outlet,
						parserOutputList,
						key );
				}
			break;

		default:
			return NFALSE;
	}
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceMFI_Outlet_NOutletList_BuildParserOutputList( NOutletList *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot )
{
	// Iterator
	NMFIOutletListService serviceType = (NMFIOutletListService)0;

	// Iterate
	for( ; serviceType < NMFI_OUTLET_LIST_SERVICES; serviceType++ )
		NDeviceMFI_Outlet_NOutletList_BuildParserOutputListInternal( this,
			parserOutputList,
			serviceType,
			keyRoot );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceMFI_Outlet_NOutletList_ProcessRESTGETRequest( NOutletList *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service type
	NMFIOutletListService serviceType;

	// Outlet
	const NOutlet *outlet;

	// Outlet index
	NU32 outletIndex;

	// Result
	__OUTPUT NBOOL result;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_list );

	// Process service
	switch( serviceType = NDeviceMFI_Service_NMFIOutletListService_FindService( requestedElement,
		cursor,
		this,
		&outletIndex ) )
	{
		case NMFI_OUTLET_LIST_SERVICE_ROOT:
			// Process
			result = NDeviceMFI_Outlet_NOutletList_BuildParserOutputList( this,
				parserOutputList,
				"" );
			break;

		case NMFI_OUTLET_LIST_SERVICE_OUTLET:
			// Get outlet
			if( !( outlet = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				outletIndex ) ) )
			{
				// Unlock
				NLib_Memoire_NListe_DesactiverProtection( this->m_list );

				// Notify
				NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

				// Quit
				return NFALSE;
			}

			// Process
			result = NDeviceMFI_Outlet_NOutlet_ProcessRESTGETRequest( outlet,
				parserOutputList,
				requestedElement,
				cursor );
			break;

		default:
			// Process
			result = NDeviceMFI_Outlet_NOutletList_BuildParserOutputListInternal( this,
				parserOutputList,
				serviceType,
				"" );
			break;
	}



	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_list );

	// OK?
	return result;
}

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param userData
 * 		The user data
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceMFI_Outlet_NOutletList_ProcessRESTPOSTRequest( NOutletList *this,
	NClientHTTP *httpClient,
	const NParserOutputList *userData,
	const char *requestedElement,
	NU32 *cursor )
{
	// Result
	__OUTPUT NBOOL result = NFALSE;

	// Outlet index
	NU32 outletIndex;

	// Outlet
	NOutlet *outlet;

	// Lock
	NLib_Memoire_NListe_ActiverProtection( this->m_list );

	switch( NDeviceMFI_Service_NMFIOutletListService_FindService( requestedElement,
		cursor,
		this,
		&outletIndex ) )
	{
		case NMFI_OUTLET_LIST_SERVICE_OUTLET:
			// Get outlet
			if( ( outlet = (NOutlet*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				outletIndex ) ) != NULL )
				result = NDeviceMFI_Outlet_NOutlet_ProcessRESTPOSTRequest( outlet,
					httpClient,
					userData,
					requestedElement,
					cursor );
			break;

		default:
			break;
	}

	// Unlock
	NLib_Memoire_NListe_DesactiverProtection( this->m_list );

	// OK?
	return result;
}
