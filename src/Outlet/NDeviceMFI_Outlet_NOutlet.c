#include "../../include/NDeviceMFI.h"

// ----------------------------------
// struct NDeviceMFI::Outlet::NOutlet
// ----------------------------------

/**
 * Build outlet
 *
 * @param outletParseData
 * 		The outlet data
 *
 * @return the outlet instance
 */
__ALLOC NOutlet *NDeviceMFI_Outlet_NOutlet_Build( const NOutletParseData *outletParseData )
{
	// Output
	__OUTPUT NOutlet *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NOutlet ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Copy data
	memcpy( out,
		outletParseData,
		sizeof( NOutlet ) );

	// OK
	return out;
}

/**
 * Destroy outlet
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutlet_Destroy( NOutlet **this )
{
	// Free
	NFREE( (*this)->m_name );
	NFREE( (*this)->m_id );
	NFREE( (*this)->m_model );
	NFREE( (*this) );
}

/**
 * Get name
 *
 * @param this
 * 		This instance
 *
 * @return the outlet name
 */
const char *NDeviceMFI_Outlet_NOutlet_GetName( const NOutlet *this )
{
	return this->m_name;
}

/**
 * Get port
 *
 * @param this
 * 		This instance
 *
 * @return the outlet port
 */
NU32 NDeviceMFI_Outlet_NOutlet_GetPort( const NOutlet *this )
{
	return this->m_portIndex;
}

/**
 * Process outlet operation
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param isEnabled
 * 		Is outlet enabled?
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_ProcessOutletOperation( NOutlet *this,
	NClientHTTP *httpClient,
	NBOOL isEnabled )
{
	// Request
	NRequeteHTTP *request;

	// Path
	char path[ 128 ];

	// Data
	char data[ 128 ];

	// Build path
	snprintf( path,
		128,
		"/sensors/%d/output",
		this->m_portIndex );

	// Build request
	if( !( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_PUT,
		path ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add host
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
		NHTTP_Client_NClientHTTP_ObtenirIPServeur( httpClient ) );

	// Add user agent
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_USER_AGENT,
		"MFIController" );

	// Add accept
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT,
		"*/*" );

	// Add cookie
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_COOKIE,
		NDEVICE_MFI_AIROS_SESSION_ID_CONSTANT"="NDEVICE_MFI_DEFAULT_AIROS_SESSION_ID );

	// Add content type
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_TYPE,
		"application/x-www-form-urlencoded" );

	// Add data
	snprintf( data,
		128,
		"output=%d",
		isEnabled );
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( request,
		data,
		(NU32)strlen( data ) );

	// Send request
	return NHTTP_Client_NClientHTTP_EnvoyerRequete( httpClient,
		request,
		NDEVICE_MFI_LOGIN_CONNECTION_TIMEOUT );
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_BuildParserOutputListInternal( const NOutlet *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot,
	NMFIOutletService serviceType )
{
	// Key
	char key[ 128 ];

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
   			: "",
		NDeviceMFI_Service_NMFIOutletService_GetName( serviceType ) );

	// Process
	switch( serviceType )
	{
		case NMFI_OUTLET_SERVICE_PORT_INDEX:
			return NParser_Output_NParserOutputList_AddEntryInteger( parserOutputList,
				key,
				this->m_portIndex );
		case NMFI_OUTLET_SERVICE_IS_ON:
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				this->m_output );
		case NMFI_OUTLET_SERVICE_ID:
			if( this->m_id != NULL )
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_id );
			else
				return NFALSE;
		case NMFI_OUTLET_SERVICE_NAME:
			if( this->m_name != NULL )
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_name );
			else
				return NFALSE;
		case NMFI_OUTLET_SERVICE_MODEL:
			if( this->m_model != NULL )
				return NParser_Output_NParserOutputList_AddEntryString( parserOutputList,
					key,
					this->m_model );
			else
				return NFALSE;
		case NMFI_OUTLET_SERVICE_POWER:
			return NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
				key,
				this->m_power );
		case NMFI_OUTLET_SERVICE_ENERGY:
			return NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
				key,
				this->m_energy );
		case NMFI_OUTLET_SERVICE_CURRENT:
			return NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
				key,
				this->m_current );
		case NMFI_OUTLET_SERVICE_VOLTAGE:
			return NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
				key,
				this->m_voltage );
		case NMFI_OUTLET_SERVICE_POWERFACTOR:
			return NParser_Output_NParserOutputList_AddEntryDouble( parserOutputList,
				key,
				this->m_powerFactor );
		case NMFI_OUTLET_SERVICE_RELAY:
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				this->m_relay );
		case NMFI_OUTLET_SERVICE_LOCK:
			return NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				this->m_lock );

		default:
			return NFALSE;
	}
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_BuildParserOutputList( const NOutlet *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot )
{
	// Service
	NMFIOutletService serviceType = (NMFIOutletService)0;

	// Iterate
	for( ; serviceType < NMFI_OUTLET_SERVICES; serviceType++ )
		NDeviceMFI_Outlet_NOutlet_BuildParserOutputListInternal( this,
			parserOutputList,
			keyRoot,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_ProcessRESTGETRequest( const NOutlet *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service
	NMFIOutletService serviceType;

	// Process service
	switch( serviceType = NDeviceMFI_Service_NMFIOutletService_FindService( requestedElement,
		cursor ) )
	{
		case NMFI_OUTLET_SERVICE_ROOT:
			return NDeviceMFI_Outlet_NOutlet_BuildParserOutputList( this,
				parserOutputList,
				"" );

		default:
			return NDeviceMFI_Outlet_NOutlet_BuildParserOutputListInternal( this,
				parserOutputList,
				"",
				serviceType );
	}
}

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param userData
 * 		The user data
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_ProcessRESTPOSTRequest( NOutlet *this,
	NClientHTTP *httpClient,
	const NParserOutputList *userData,
	const char *requestedElement,
	NU32 *cursor )
{
	// Parser output
	const NParserOutput *parserOutput;

	// Service type
	NMFIOutletService serviceType;

	// Process service
	switch( serviceType = NDeviceMFI_Service_NMFIOutletService_FindService( requestedElement,
		cursor ) )
	{
		case NMFI_OUTLET_SERVICE_IS_ON:
			// Get parser output entry
			if( ( parserOutput = NParser_Output_NParserOutputList_GetFirstOutput( userData,
				NDeviceMFI_Service_NMFIOutletService_GetName( serviceType ),
				NFALSE,
				NTRUE ) ) != NULL
				&& NParser_Output_NParserOutput_GetType( parserOutput ) == NPARSER_OUTPUT_TYPE_BOOLEAN )
				// Process
				return NDeviceMFI_Outlet_NOutlet_ProcessOutletOperation( this,
					httpClient,
					*(NBOOL*)NParser_Output_NParserOutput_GetValue( parserOutput ) );
			break;

		default:
			break;
	}

	// Bad request
	return NFALSE;
}
