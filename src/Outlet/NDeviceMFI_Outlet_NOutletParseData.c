#include "../../include/NDeviceMFI.h"

// -------------------------------------------
// struct NDeviceMFI::Outlet::NOutletParseData
// -------------------------------------------

/**
 * Destroy
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletParseData_Free( NOutletParseData *this )
{
	// Free
	NFREE( this->m_model );
	NFREE( this->m_id );
	NFREE( this->m_name );

	// Zero
	memset( this,
		0,
		sizeof( NOutletParseData ) );
}

/**
 * Read port data
 *
 * @param parserOutputList
 * 		The parser output list
 * @param cursor
 * 		The cursor in parser output list data
 * @param result
 * 		The result
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutletParseData_ReadPortData( const NParserOutputList *parserOutputList,
	NU32 *cursor,
	NOutletParseData *parseData )
{
	// Is must read port
	NBOOL isMustReadPort = NTRUE;

	// Service type
	NMFIOutletDeviceService serviceType;

	// Is must continue?
	NBOOL isMustContinue = NTRUE;

	// Parser output
	const NParserOutput *parserOutput;

	// Empty parse data
	memset( parseData,
		0,
		sizeof( NOutletParseData ) );

	// Read
	while( isMustContinue )
	{
		// Read output
		if( !( parserOutput = NParser_Output_NParserOutputList_GetEntry( parserOutputList,
			(*cursor) ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Free
			NDeviceMFI_Outlet_NOutletParseData_Free( parseData );

			// Quit
			return NFALSE;
		}

		// Find service
		serviceType = NDevice_Outlet_NMFIOutletDeviceService_FindService( NParser_Output_NParserOutput_GetKey( parserOutput ) );

		// Check data type
		if( !NDevice_Outlet_NMFIOutletDeviceService_IsCorrectDataType( serviceType,
			NParser_Output_NParserOutput_GetType( parserOutput ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			NParser_Output_NParserOutput_Display( parserOutput );

			// Free
			NDeviceMFI_Outlet_NOutletParseData_Free( parseData );

			// Quit
			return NFALSE;
		}

		// Process
		switch( serviceType )
		{
			case NMFI_OUTLET_DEVICE_SERVICE_PORT:
				// Do we read the current port
				if( isMustReadPort )
				{
					// Read port
					parseData->m_portIndex = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput );

					// Done
					isMustReadPort = NFALSE;
				}
				// Are we at the next port?
				else
					// Done
					return NTRUE;
				break;

			case NMFI_OUTLET_DEVICE_SERVICE_ID:
				parseData->m_id = NParser_Output_NParserOutput_GetValueCopy( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_NAME:
				parseData->m_name = NParser_Output_NParserOutput_GetValueCopy( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_MODEL:
				parseData->m_model = NParser_Output_NParserOutput_GetValueCopy( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_OUTPUT:
				parseData->m_output = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) == 0 ?
					NFALSE
					: NTRUE;
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_POWER:
				parseData->m_power = *(double*)NParser_Output_NParserOutput_GetValue( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_ENERGY:
				parseData->m_energy = *(double*)NParser_Output_NParserOutput_GetValue( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_ENABLED:
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_CURRENT:
				parseData->m_current = *(double*)NParser_Output_NParserOutput_GetValue( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_VOLTAGE:
				parseData->m_voltage = *(double*)NParser_Output_NParserOutput_GetValue( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_POWER_FACTOR:
				parseData->m_powerFactor = *(double*)NParser_Output_NParserOutput_GetValue( parserOutput );
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_RELAY:
				parseData->m_relay = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) == 0 ?
					NFALSE
					: NTRUE;
				break;
			case NMFI_OUTLET_DEVICE_SERVICE_STATE_LOCK:
				parseData->m_lock = *(NU32*)NParser_Output_NParserOutput_GetValue( parserOutput ) == 0 ?
					NFALSE
					: NTRUE;
				break;

			case NMFI_OUTLET_DEVICE_SERVICE_STATUS:
				isMustContinue = NFALSE;
				break;

			default:
				// Notify
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// Free
				NDeviceMFI_Outlet_NOutletParseData_Free( parseData );

				// Quit
				return NFALSE;
		}

		// Increment
		(*cursor)++;
	}

	// OK
	return NTRUE;
}