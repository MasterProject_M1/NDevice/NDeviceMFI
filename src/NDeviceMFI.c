#include "../include/NDeviceMFI.h"

// --------------------
// namespace NDeviceMFI
// --------------------

#ifdef NDEVICE_MFI_TEST_PROJECT
/**
 * Entry point
 *
 * @param argc
 * 		The arguments count
 * @param argv
 * 		The arguments vector
 *
 * @return EXIT_SUCCESS if ok, EXIT_FAILURE else
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// mFi device
	NDeviceMFI *deviceMFI;

	// Init NLib
	NLib_Initialiser( NULL );

	// Build mfi device
	if( !( deviceMFI = NDeviceMFI_NDeviceMFI_Build( "192.168.43.241",
		80 ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit NLib
		NLib_Detruire( );

		// Quit
		return EXIT_FAILURE;
	}

	// Wait
	while( NTRUE )
	{
		NLib_Temps_Attendre( 5000 );

		if( NDeviceMFI_NDeviceMFI_ActivateOutlet( deviceMFI,
			0 ) )
			puts( "J'active la prise 1" );

		NLib_Temps_Attendre( 5000 );


		if( NDeviceMFI_NDeviceMFI_DeactivateOutlet( deviceMFI,
			0 ) )
			puts( "Je desactive la prise 1" );
	}

	// Destroy mfi devices
	NDeviceMFI_NDeviceMFI_Destroy( &deviceMFI );

	// Quit NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
#endif // NDEVICE_MFI_TEST_PROJECT
