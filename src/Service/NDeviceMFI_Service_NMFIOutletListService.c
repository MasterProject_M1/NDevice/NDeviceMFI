#define NDEVICEMFI_SERVICE_NMFIOUTLETLISTSERVICE_INTERNE
#include "../../include/NDeviceMFI.h"

// -----------------------------------------------
// enum NDeviceMFI::Service::NMFIOutletListService
// -----------------------------------------------

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param outletList
 * 		The outlet list
 * @param outletIndex
 * 		The output outlet index
 *
 * @return the service type
 */
NMFIOutletListService NDeviceMFI_Service_NMFIOutletListService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED const NOutletList *outletList,
	__OUTPUT NU32 *outletIndex )
{
	// Buffer
	char *buffer;

	// Service
	__OUTPUT NMFIOutletListService serviceType;

	// Read element
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NMFI_OUTLET_LIST_SERVICES;
	}

	// Look for
	for( serviceType = (NMFIOutletListService)0; serviceType < NMFI_OUTLET_LIST_SERVICES; serviceType++ )
		switch( serviceType )
		{
			case NMFI_OUTLET_LIST_SERVICE_OUTLET:
				// ID ?
				if( NLib_Chaine_EstUnNombreEntier( buffer,
					10 ) )
				{
					// Check if this is a valid index
					if( ( *outletIndex = (NU32)strtol( buffer,
						NULL,
						10 ) ) < NDeviceMFI_Outlet_NOutletList_GetOutletCount( outletList ) )
					{
						// Free
						NFREE( buffer );

						// OK
						return serviceType;
					}
				}
				// Name?
				else
					// Look for name
					if( ( *outletIndex = NDeviceMFI_Outlet_NOutletList_GetOutletIndexFromName( outletList,
						buffer ) ) != NERREUR )
					{
						// Free
						NFREE( buffer );

						// OK
						return serviceType;
					}
				break;

			default:
				// Check
				if( NLib_Chaine_Comparer( buffer,
					NMFIOutletListServiceName[ serviceType ],
					NTRUE,
					0 ) )
				{
					// Free
					NFREE( buffer );

					// Found it!
					return serviceType;
				}
				break;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NMFI_OUTLET_LIST_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceMFI_Service_NMFIOutletListService_GetName( NMFIOutletListService serviceType )
{
	return NMFIOutletListServiceName[ serviceType ];
}
