#define NDEVICEMFI_SERVICE_NMFISERVICE_INTERNE
#include "../../include/NDeviceMFI.h"

// -------------------------------------
// enum NDeviceMFI::Service::NMFIService
// -------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NMFIService NDeviceMFI_Service_NMFIService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Output
	__OUTPUT NMFIService serviceType;

	// Buffer
	char *buffer;

	// Read element
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NMFI_SERVICES;
	}

	// Look for
	for( serviceType = (NMFIService)0; serviceType < NMFI_SERVICES; serviceType++ )
		// Check
		if( NLib_Chaine_Comparer( NMFIServiceName[ serviceType ],
			buffer,
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// OK
			return serviceType;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NMFI_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceMFI_Service_NMFIService_GetName( NMFIService serviceType )
{
	return NMFIServiceName[ serviceType ];
}
