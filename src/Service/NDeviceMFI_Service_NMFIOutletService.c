#define NDEVICEMFI_SERVICE_NMFIOUTLETSERVICE_INTERNE
#include "../../include/NDeviceMFI.h"

// -------------------------------------------
// enum NDeviceMFI::Service::NMFIOutletService
// -------------------------------------------

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NMFIOutletService NDeviceMFI_Service_NMFIOutletService_FindService( const char *requestedElement,
	NU32 *cursor )
{
	// Service
	NMFIOutletService serviceType;

	// Buffer
	char *buffer;

	// Read element
	if( !( buffer = NLib_Chaine_LireJusqua( requestedElement,
		'/',
		cursor,
		NFALSE ) ) )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quit
		return NMFI_OUTLET_SERVICES;
	}

	// Look for
	for( serviceType = (NMFIOutletService)0; serviceType < NMFI_OUTLET_SERVICES; serviceType++ )
		// Check
		if( NLib_Chaine_Comparer( NMFIOutletServiceName[ serviceType ],
			buffer,
			NTRUE,
			0 ) )
		{
			// Free
			NFREE( buffer );

			// Found it!
			return serviceType;
		}

	// Free
	NFREE( buffer );

	// Not found
	return NMFI_OUTLET_SERVICES;
}

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service type
 */
const char *NDeviceMFI_Service_NMFIOutletService_GetName( NMFIOutletService serviceType )
{
	return NMFIOutletServiceName[ serviceType ];
}
