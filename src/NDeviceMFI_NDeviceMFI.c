#include "../include/NDeviceMFI.h"

// -----------------------------
// struct NDeviceMFI::NDeviceMFI
// -----------------------------

/**
 * Receive callback
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The receiver
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_ReceiveCallback( const NPacket *packet,
	const NClient *client )
{
	// Device MFI
	NDeviceMFI *this;

	// HTTP client
	NClientHTTP *httpClient;

	// Response
	NReponseHTTP *response;

	// Parser output list
	NParserOutputList *parserOutputList;

	// Secure data
	char *secureData;

	// Cursor
	NU32 cursor = 0;

	// Header
	const char *header;

	// Get this instance
	if( !( httpClient = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		|| !( this = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( httpClient ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Secure data
		// Allocate
			if( !( secureData = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
				sizeof( char ) ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Authentication not sent anymore
				this->m_isAuthenticationSent = NFALSE;

				// Quit
				return NFALSE;
			}
		// Copy
			memcpy( secureData,
				NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
				NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Parse http response
	if( !( response = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( secureData ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Authentication not sent anymore
		this->m_isAuthenticationSent = NFALSE;

		// Free
		NFREE( secureData );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( secureData );

	// Authentication attempt?
	if( this->m_isAuthenticationSent )
	{
		// Check code
		if( NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCode( response ) != NHTTP_CODE_302_FOUND_DOCUMENT )
		{
			// Deactivate authentication flag
			this->m_isAuthenticationSent = NFALSE;

			// Notify
			NOTIFIER_ERREUR( NERREUR_AUTH_FAILED );

			// Destroy
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

			// Quit
			return NFALSE;
		}

		// Is location header set to /index.cgi
		if( !( header = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( response,
			NMOT_CLEF_REPONSE_HTTP_LOCATION ) )
			|| !NLib_Chaine_Comparer( header,
				"/index.cgi",
				NTRUE,
				0 ) )
		{
			// Deactivate authentication flag
			this->m_isAuthenticationSent = NFALSE;

			// Notify
			NOTIFIER_ERREUR( NERREUR_AUTH_FAILED );

			// Destroy
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

			// Quit
			return NFALSE;
		}

		// We're authenticated
		this->m_isAuthenticated = NTRUE;
		this->m_isAuthenticationSent = NFALSE;
	}
	else
	{
		// Secure data
		if( ( secureData = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( response ) ) != NULL )
		{
			// Remove ending 0
			for( cursor = (NU32)strlen( secureData ); cursor > 0; cursor-- )
				// Break?
				if( secureData[ cursor ] == '}' )
					break;
				// Remove character
				else
					secureData[ cursor ] = '\0';

			// Look for '{'
			cursor = 0;
			if( !NLib_Chaine_PlacerAuCaractere( secureData,
				'{',
				&cursor ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_SYNTAX );

				// Free
				NFREE( secureData );

				// Destroy
				NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

				// Quit
				return NFALSE;
			}

			// Move left
			if( cursor > 0 )
				cursor--;

			// Parse
			if( ( parserOutputList = NJson_Engine_Parser_Parse( secureData + cursor,
				(NU32)strlen( secureData ) - cursor ) ) != NULL )
			{
				// Update outlet list
				NDeviceMFI_Outlet_NOutletList_Update( this->m_outletList,
					parserOutputList );

				// Destroy parser output list
				NParser_Output_NParserOutputList_Destroy( &parserOutputList );
			}
			else
			{
				// We are no more authenticated
				this->m_isAuthenticated = NFALSE;
				this->m_isAuthenticationSent = NFALSE;
			}

			// Free
			NFREE( secureData );
		}

	}

	// Destroy response
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

	// OK
	return NTRUE;
}

/**
 * Update thread
 *
 * @param this
 * 		This instance
 *
 * @return if the operation succeeded
 */
__PRIVATE __THREAD NBOOL NDeviceMFI_NDeviceMFI_UpdateThread( NDeviceMFI *this )
{
	// Request
	NRequeteHTTP *request;

	// Login
	char login[ 256 ];

	// Thread
	do
	{
		// Not authenticated
		if( !this->m_isAuthenticated )
		{
			// Authentication to be sent?
			if( !this->m_isAuthenticationSent )
			{
				// Sent
				this->m_isAuthenticationSent = NTRUE;
				this->m_authenticationSendingTime = NLib_Temps_ObtenirTick64( );

				// Build request
				if( ( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_POST,
					"/login.cgi" ) ) != NULL )
				{
					// Add host
					NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
						NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
						this->m_hostname );

					// Add user agent
					NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
						NMOT_CLEF_REQUETE_HTTP_KEYWORD_USER_AGENT,
						"MFIController" );

					// Add accept
					NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
						NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT,
						"*/*" );

					// Add cookie
					NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
						NMOT_CLEF_REQUETE_HTTP_KEYWORD_COOKIE,
						NDEVICE_MFI_AIROS_SESSION_ID_CONSTANT"="NDEVICE_MFI_DEFAULT_AIROS_SESSION_ID );

					// Add content type
					NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
						NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_TYPE,
						"application/x-www-form-urlencoded" );

					// Build login
					snprintf( login,
						256,
						"username=%s&password=%s",
						this->m_username,
						this->m_password );

					// Add login
					NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( request,
						login,
						(NU32)strlen( login ) );

					// Send authentication
					NHTTP_Client_NClientHTTP_EnvoyerRequete( this->m_updateHTTPClient,
						request,
						NDEVICE_MFI_LOGIN_CONNECTION_TIMEOUT );
				}
			}
			else
				// Timeout?
				if( NLib_Temps_ObtenirTick64( ) - this->m_authenticationSendingTime >= NDEVICE_MFI_LOGIN_AUTHENTICATION_SENT_TIMEOUT )
					// Not sent anymore
					this->m_isAuthenticationSent = NFALSE;
		}
		// Authenticated
		else
		{
			// Build sensors request
			if( ( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_GET,
				"/sensors" ) ) != NULL )
			{
				// Add host
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
					NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
					this->m_hostname );

				// Add user agent
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
					NMOT_CLEF_REQUETE_HTTP_KEYWORD_USER_AGENT,
					"MFIController" );

				// Add accept
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
					NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT,
					"*/*" );

				// Add cookie
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
					NMOT_CLEF_REQUETE_HTTP_KEYWORD_COOKIE,
					NDEVICE_MFI_AIROS_SESSION_ID_CONSTANT"="NDEVICE_MFI_DEFAULT_AIROS_SESSION_ID );

				// Send request
				NHTTP_Client_NClientHTTP_EnvoyerRequete( this->m_updateHTTPClient,
					request,
					NDEVICE_MFI_LOGIN_CONNECTION_TIMEOUT );
			}
		}

		// Wait
		NLib_Temps_Attendre( NDEVICE_MFI_DELAY_BETWEEN_UPDATE );
	} while( this->m_isUpdateThreadRunning );

	// OK
	return NTRUE;
}

/**
 * Build MFI device
 *
 * @param hostname
 * 		The hostname
 * @param port
 * 		The port
 *
 * @return the mfi built instance
 */
__ALLOC NDeviceMFI *NDeviceMFI_NDeviceMFI_Build( const char *hostname,
	NU32 port )
{
	// Build with default login
	return NDeviceMFI_NDeviceMFI_Build2( hostname,
		port,
		NDEVICE_MFI_DEFAULT_LOGIN_USERNAME,
		NDEVICE_MFI_DEFAULT_LOGIN_PASSWORD );
}

/**
 * Build MFI device
 *
 * @param hostname
 * 		The hostname
 * @param port
 * 		The port
 * @param username
 * 		The username
 * @param password
 * 		The password
 *
 * @return the mfi built instance
 */
__ALLOC NDeviceMFI *NDeviceMFI_NDeviceMFI_Build2( const char *hostname,
	NU32 port,
	const char *username,
	const char *password )
{
	// Output
	NDeviceMFI *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NDeviceMFI ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Save properties
	out->m_port = port;
	if( !( out->m_hostname = NLib_Chaine_Dupliquer( hostname ) )
		|| !( out->m_username = NLib_Chaine_Dupliquer( username ) )
		|| !( out->m_password = NLib_Chaine_Dupliquer( password ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_username );
		NFREE( out->m_hostname );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build outlet list
	if( !( out->m_outletList = NDeviceMFI_Outlet_NOutletList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_password );
		NFREE( out->m_username );
		NFREE( out->m_hostname );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build http clients
		// Update client
			if( !( out->m_updateHTTPClient = NHTTP_Client_NClientHTTP_Construire3( out->m_hostname,
				out->m_port,
				out,
				NDeviceMFI_NDeviceMFI_ReceiveCallback,
				NDEVICE_MFI_RECEIVE_TIMEOUT ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Destroy
				NDeviceMFI_Outlet_NOutletList_Destroy( &out->m_outletList );

				// Free
				NFREE( out->m_password );
				NFREE( out->m_username );
				NFREE( out->m_hostname );
				NFREE( out );

				// Quit
				return NULL;
			}
		// Output client
			if( !( out->m_outputHTTPClient = NHTTP_Client_NClientHTTP_Construire3( out->m_hostname,
				out->m_port,
				out,
				NHTTP_Client_Outil_CallbackReception,
				NDEVICE_MFI_RECEIVE_TIMEOUT ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Destroy http client
				NHTTP_Client_NClientHTTP_Detruire( &out->m_updateHTTPClient );

				// Destroy
				NDeviceMFI_Outlet_NOutletList_Destroy( &out->m_outletList );

				// Free
				NFREE( out->m_password );
				NFREE( out->m_username );
				NFREE( out->m_hostname );
				NFREE( out );

				// Quit
				return NULL;
			}

	// Start thread
	out->m_isUpdateThreadRunning = NTRUE;
	if( !( out->m_updateThread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NDeviceMFI_NDeviceMFI_UpdateThread,
		out,
		&out->m_isUpdateThreadRunning ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy http clients
		NHTTP_Client_NClientHTTP_Detruire( &out->m_outputHTTPClient );
		NHTTP_Client_NClientHTTP_Detruire( &out->m_updateHTTPClient );

		// Destroy
		NDeviceMFI_Outlet_NOutletList_Destroy( &out->m_outletList );

		// Free
		NFREE( out->m_password );
		NFREE( out->m_username );
		NFREE( out->m_hostname );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy mfi instance
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_NDeviceMFI_Destroy( NDeviceMFI **this )
{
	// Destroy update thread
	NLib_Thread_NThread_Detruire( &(*this)->m_updateThread );

	// Destroy http clients
	NHTTP_Client_NClientHTTP_Detruire( &(*this)->m_outputHTTPClient );
	NHTTP_Client_NClientHTTP_Detruire( &(*this)->m_updateHTTPClient );

	// Destroy outlet list
	NDeviceMFI_Outlet_NOutletList_Destroy( &(*this)->m_outletList );

	// Free
	NFREE( (*this)->m_hostname );
	NFREE( (*this)->m_username );
	NFREE( (*this)->m_password );
	NFREE( (*this) );
}

/**
 * Process outlet operation
 *
 * @param this
 * 		This instance
 * @param index
 * 		The outlet index
 * @param isEnable
 * 		Is outlet enable?
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NDeviceMFI_NDeviceMFI_ProcessOutletOperation( NDeviceMFI *this,
	NU32 index,
	NBOOL isEnable )
{
	// Result
	__OUTPUT NBOOL result;

	// Lock
	NDeviceMFI_Outlet_NOutletList_ActivateProtection( this->m_outletList );

	// Process
	result = NDeviceMFI_Outlet_NOutletList_ProcessOutletOperation( this->m_outletList,
		this->m_outputHTTPClient,
		index,
		isEnable );

	// Unlock
	NDeviceMFI_Outlet_NOutletList_DeactivateProtection( this->m_outletList );

	// OK?
	return result;
}

/**
 * Activate outlet
 *
 * @param this
 * 		This instance
 * @param index
 * 		The outlet index
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_ActivateOutlet( NDeviceMFI *this,
	NU32 index )
{
	return NDeviceMFI_NDeviceMFI_ProcessOutletOperation( this,
		index,
		NTRUE );
}

/**
 * Deactivate outlet
 *
 * @param this
 * 		This instance
 * @param index
 * 		The outlet index
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_DeactivateOutlet( NDeviceMFI *this,
	NU32 index )
{
	return NDeviceMFI_NDeviceMFI_ProcessOutletOperation( this,
		index,
		NFALSE );
}

/**
 * Build parser output list internal
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 * @param serviceType
 * 		The service type
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NDeviceMFI_NDeviceMFI_BuildParserOutputListInternal( NDeviceMFI *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot,
	NMFIService serviceType )
{
	// Key
	char key[ 128 ];

	// Build key
	snprintf( key,
		128,
		"%s%s%s",
		keyRoot,
		strlen( keyRoot ) > 0 ?
			"."
   			: "",
		NDeviceMFI_Service_NMFIService_GetName( serviceType ) );

	// Process
	switch( serviceType )
	{
		case NMFI_SERVICE_IS_AUTHENTICATED:
			NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				this->m_isAuthenticated );
			break;
		case NMFI_SERVICE_IS_AUTHENTICATION_REQUEST_SENT:
			NParser_Output_NParserOutputList_AddEntryBoolean( parserOutputList,
				key,
				this->m_isAuthenticationSent );
			break;
		case NMFI_SERVICE_OUTLET_LIST:
			// Lock
			NDeviceMFI_Outlet_NOutletList_ActivateProtection( this->m_outletList );

			// Process
			NDeviceMFI_Outlet_NOutletList_BuildParserOutputList( this->m_outletList,
				parserOutputList,
				key );

			// Unlock
			NDeviceMFI_Outlet_NOutletList_DeactivateProtection( this->m_outletList );
			break;

		default:
			return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_BuildParserOutputList( NDeviceMFI *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot )
{
	// Iterator
	NMFIService serviceType = (NMFIService)0;

	// Iterate
	for( ; serviceType < NMFI_SERVICES; serviceType++ )
		// Add service
		NDeviceMFI_NDeviceMFI_BuildParserOutputListInternal( this,
			parserOutputList,
			keyRoot,
			serviceType );

	// OK
	return NTRUE;
}

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_ProcessRESTGETRequest( NDeviceMFI *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor )
{
	// Service type
	NMFIService serviceType;

	// Process service
	switch( serviceType = NDeviceMFI_Service_NMFIService_FindService( requestedElement,
		cursor ) )
	{
		case NMFI_SERVICE_ROOT:
			return NDeviceMFI_NDeviceMFI_BuildParserOutputList( this,
				parserOutputList,
				"" );

		case NMFI_SERVICE_OUTLET_LIST:
			return NDeviceMFI_Outlet_NOutletList_ProcessRESTGETRequest( this->m_outletList,
				parserOutputList,
				requestedElement,
				cursor );

		default:
			return NDeviceMFI_NDeviceMFI_BuildParserOutputListInternal( this,
				parserOutputList,
				"",
				serviceType );
	}
}

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param userData
 * 		The user data
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_ProcessRESTPOSTRequest( NDeviceMFI *this,
	const NParserOutputList *userData,
	const char *requestedElement,
	NU32 *cursor )
{
	// Process service
	switch( NDeviceMFI_Service_NMFIService_FindService( requestedElement,
		cursor ) )
	{
		case NMFI_SERVICE_OUTLET_LIST:
			return NDeviceMFI_Outlet_NOutletList_ProcessRESTPOSTRequest( this->m_outletList,
				this->m_outputHTTPClient,
				userData,
				requestedElement,
				cursor );

		default:
			return NFALSE;
	}
}

/**
 * Get outlet list
 *
 * @param this
 * 		This instance
 *
 * @return the outlet list
 */
NOutletList *NDeviceMFI_NDeviceMFI_GetOutletList( NDeviceMFI *this )
{
	return this->m_outletList;
}
