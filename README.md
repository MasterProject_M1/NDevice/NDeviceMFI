NDeviceMFI
==========

Introduction
------------

This device is dedicated to the control of mFi mPower devices

It can, once ip:port is set:

- GET current status of sensors at time interval
- PUT outlet on/off when desired

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NDevice/NDeviceMFI.git

