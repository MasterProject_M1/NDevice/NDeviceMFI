#ifndef NDEVICEMFI_SERVICE_NMFISERVICE_PROTECT
#define NDEVICEMFI_SERVICE_NMFISERVICE_PROTECT

// -------------------------------------
// enum NDeviceMFI::Service::NMFIService
// -------------------------------------

typedef enum NMFIService
{
	NMFI_SERVICE_ROOT,

	NMFI_SERVICE_IS_AUTHENTICATED,
	NMFI_SERVICE_IS_AUTHENTICATION_REQUEST_SENT,
	NMFI_SERVICE_OUTLET_LIST,

	NMFI_SERVICES
} NMFIService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NMFIService NDeviceMFI_Service_NMFIService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceMFI_Service_NMFIService_GetName( NMFIService serviceType );

#ifdef NDEVICEMFI_SERVICE_NMFISERVICE_INTERNE
__PRIVATE const char NMFIServiceName[ NMFI_SERVICES ][ 32 ] =
{
	"",

	"isAuthenticated",
	"isAuthenticationRequestSent",
	"list"
};
#endif // NDEVICEMFI_SERVICE_NMFISERVICE_INTERNE

#endif // !NDEVICEMFI_SERVICE_NMFISERVICE_PROTECT
