#ifndef NDEVICEMFI_SERVICE_NMFIOUTLETSERVICE_PROTECT
#define NDEVICEMFI_SERVICE_NMFIOUTLETSERVICE_PROTECT

// -------------------------------------------
// enum NDeviceMFI::Service::NMFIOutletService
// -------------------------------------------

typedef enum NMFIOutletService
{
	NMFI_OUTLET_SERVICE_ROOT,

	NMFI_OUTLET_SERVICE_PORT_INDEX,
	NMFI_OUTLET_SERVICE_IS_ON,
	NMFI_OUTLET_SERVICE_ID,
	NMFI_OUTLET_SERVICE_NAME,
	NMFI_OUTLET_SERVICE_MODEL,
	NMFI_OUTLET_SERVICE_POWER,
	NMFI_OUTLET_SERVICE_ENERGY,
	NMFI_OUTLET_SERVICE_CURRENT,
	NMFI_OUTLET_SERVICE_VOLTAGE,
	NMFI_OUTLET_SERVICE_POWERFACTOR,
	NMFI_OUTLET_SERVICE_RELAY,
	NMFI_OUTLET_SERVICE_LOCK,

	NMFI_OUTLET_SERVICES
} NMFIOutletService;

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return the service type
 */
NMFIOutletService NDeviceMFI_Service_NMFIOutletService_FindService( const char *requestedElement,
	NU32 *cursor );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service type
 */
const char *NDeviceMFI_Service_NMFIOutletService_GetName( NMFIOutletService serviceType );

#ifdef NDEVICEMFI_SERVICE_NMFIOUTLETSERVICE_INTERNE
__PRIVATE const char NMFIOutletServiceName[ NMFI_OUTLET_SERVICES ][ 32 ] =
{
	"",

	"index",
	"isOn",
	"id",
	"name",
	"model",
	"power",
	"energy",
	"current",
	"voltage",
	"powerFactor",
	"relay",
	"lock"
};
#endif // NDEVICEMFI_SERVICE_NMFIOUTLETSERVICE_INTERNE

#endif // !NDEVICEMFI_SERVICE_NMFIOUTLETSERVICE_PROTECT
