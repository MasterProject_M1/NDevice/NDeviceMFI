#ifndef NDEVICEMFI_SERVICE_NMFIOUTLETLISTSERVICE_PROTECT
#define NDEVICEMFI_SERVICE_NMFIOUTLETLISTSERVICE_PROTECT

// -----------------------------------------------
// enum NDeviceMFI::Service::NMFIOutletListService
// -----------------------------------------------

typedef enum NMFIOutletListService
{
	NMFI_OUTLET_LIST_SERVICE_ROOT,

	NMFI_OUTLET_LIST_SERVICE_COUNT,
	NMFI_OUTLET_LIST_SERVICE_OUTLET,

	NMFI_OUTLET_LIST_SERVICES
} NMFIOutletListService;

/**
 * Find service type
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param outletList
 * 		The outlet list
 * @param outletIndex
 * 		The output outlet index
 *
 * @return the service type
 */
NMFIOutletListService NDeviceMFI_Service_NMFIOutletListService_FindService( const char *requestedElement,
	NU32 *cursor,
	__MUSTBEPROTECTED const NOutletList *outletList,
	__OUTPUT NU32 *outletIndex );

/**
 * Get service name
 *
 * @param serviceType
 * 		The service type
 *
 * @return the service name
 */
const char *NDeviceMFI_Service_NMFIOutletListService_GetName( NMFIOutletListService serviceType );

#ifdef NDEVICEMFI_SERVICE_NMFIOUTLETLISTSERVICE_INTERNE
__PRIVATE const char NMFIOutletListServiceName[ NMFI_OUTLET_LIST_SERVICES ][ 32 ] =
{
	"",

	"count",
	""
};
#endif // NDEVICEMFI_SERVICE_NMFIOUTLETLISTSERVICE_INTERNE

#endif // !NDEVICEMFI_SERVICE_NMFIOUTLETLISTSERVICE_PROTECT
