#ifndef NDEVICEMFI_SERVICE_PROTECT
#define NDEVICEMFI_SERVICE_PROTECT

// -----------------------------
// namespace NDeviceMFI::Service
// -----------------------------

// enum NDeviceMFI::Service::NMFIService
#include "NDeviceMFI_Service_NMFIService.h"

// enum NDeviceMFI::Service::NMFIOutletList
#include "NDeviceMFI_Service_NMFIOutletListService.h"

// enum NDeviceMFI::Service::NMFIOutlet
#include "NDeviceMFI_Service_NMFIOutletService.h"

#endif // !NDEVICEMFI_SERVICE_PROTECT
