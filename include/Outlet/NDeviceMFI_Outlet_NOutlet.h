#ifndef NDEVICEMFI_OUTLET_NOUTLET_PROTECT
#define NDEVICEMFI_OUTLET_NOUTLET_PROTECT

// ----------------------------------
// struct NDeviceMFI::Outlet::NOutlet
// ----------------------------------

typedef NOutletParseData NOutlet;

/**
 * Build outlet
 *
 * @param outletParseData
 * 		The outlet data
 *
 * @return the outlet instance
 */
__ALLOC NOutlet *NDeviceMFI_Outlet_NOutlet_Build( const NOutletParseData *outletParseData );

/**
 * Destroy outlet
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutlet_Destroy( NOutlet** );

/**
 * Get name
 *
 * @param this
 * 		This instance
 *
 * @return the outlet name
 */
const char *NDeviceMFI_Outlet_NOutlet_GetName( const NOutlet* );

/**
 * Get port
 *
 * @param this
 * 		This instance
 *
 * @return the outlet port
 */
NU32 NDeviceMFI_Outlet_NOutlet_GetPort( const NOutlet* );

/**
 * Process outlet operation
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param isEnabled
 * 		Is outlet enabled?
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_ProcessOutletOperation( NOutlet*,
	NClientHTTP *httpClient,
	NBOOL isEnabled );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_BuildParserOutputList( const NOutlet*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_ProcessRESTGETRequest( const NOutlet*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor );

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param userData
 * 		The user data
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutlet_ProcessRESTPOSTRequest( NOutlet*,
	NClientHTTP *httpClient,
	const NParserOutputList *userData,
	const char *requestedElement,
	NU32 *cursor );

#endif // !NDEVICEMFI_OUTLET_NOUTLET_PROTECT
