#ifndef NDEVICEMFI_OUTLET_PROTECT
#define NDEVICEMFI_OUTLET_PROTECT

// ----------------------------
// namespace NDeviceMFI::Outlet
// ----------------------------

// enum NDeviceMFI::Outlet::NMFIOutletService
#include "NDeviceMFI_Outlet_NMFIOutletDeviceService.h"

// struct NDeviceMFI::Outlet::NOutletParseData
#include "NDeviceMFI_Outlet_NOutletParseData.h"

// struct NDeviceMFI::Outlet::NOutlet
#include "NDeviceMFI_Outlet_NOutlet.h"

// struct NDeviceMFI::Outlet::NOutletList
#include "NDeviceMFI_Outlet_NOutletList.h"

#endif // !NDEVICEMFI_OUTLET_PROTECT
