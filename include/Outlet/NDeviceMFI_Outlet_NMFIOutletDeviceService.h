#ifndef NDEVICEMFI_OUTLET_NMFIOUTLETDEVICESERVICE_PROTECT
#define NDEVICEMFI_OUTLET_NMFIOUTLETDEVICESERVICE_PROTECT

// ------------------------------------------
// enum NDeviceMFI::Outlet::NMFIOutletService
// ------------------------------------------

typedef enum NMFIOutletDeviceService
{
	NMFI_OUTLET_DEVICE_SERVICE_PORT,
	NMFI_OUTLET_DEVICE_SERVICE_ID,
	NMFI_OUTLET_DEVICE_SERVICE_NAME,
	NMFI_OUTLET_DEVICE_SERVICE_MODEL,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_OUTPUT,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_POWER,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_ENERGY,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_ENABLED,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_CURRENT,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_VOLTAGE,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_POWER_FACTOR,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_RELAY,
	NMFI_OUTLET_DEVICE_SERVICE_STATE_LOCK,

	NMFI_OUTLET_DEVICE_SERVICE_STATUS,

	NMFI_OUTLET_DEVICE_SERVICES
} NMFIOutletDeviceService;

#define NMFI_OUTLET_DEVICE_SERVICE_SYNTAX_COUNT	2

/**
 * Find service
 *
 * @param name
 * 		The service name
 *
 * @return the service type or NMFI_OUTLET_SERVICES
 */
NMFIOutletDeviceService NDevice_Outlet_NMFIOutletDeviceService_FindService( const char *name );

/**
 * Is correct data type
 *
 * @param serviceType
 * 		The service type
 * @param parserDataType
 * 		The data type
 *
 * @return if this is the correct data type
 */
NBOOL NDevice_Outlet_NMFIOutletDeviceService_IsCorrectDataType( NMFIOutletDeviceService serviceType,
	NParserOutputType parserDataType );

#ifdef NDEVICEMFI_OUTLET_NMFIOUTLETDEVICESERVICE_INTERNE
__PRIVATE const char NMFIOutletDeviceServiceName[ NMFI_OUTLET_DEVICE_SERVICE_SYNTAX_COUNT ][ NMFI_OUTLET_DEVICE_SERVICES ][ 32 ] =
{
	{
		"sensors.port",
		"sensors.id",
		"sensors.label",
		"sensors.model",
		"sensors.state.output",
		"sensors.state.power",
		"sensors.state.energy",
		"sensors.state.enabled",
		"sensors.state.current",
		"sensors.state.voltage",
		"sensors.state.powerfactor",
		"sensors.state.relay",
		"sensors.state.lock",

		"status"
	},
	{
		"sensors.port",
		"sensors.id",
		"sensors.label",
		"sensors.model",
		"sensors.output",
		"sensors.power",
		"sensors.energy",
		"sensors.enabled",
		"sensors.current",
		"sensors.voltage",
		"sensors.powerfactor",
		"sensors.relay",
		"sensors.lock",

		"status"
	}
};

__PRIVATE const NParserOutputType NMFIOutletDeviceServiceType[ NMFI_OUTLET_DEVICE_SERVICES ] =
{
	NPARSER_OUTPUT_TYPE_INTEGER,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_INTEGER,
	NPARSER_OUTPUT_TYPE_DOUBLE,
	NPARSER_OUTPUT_TYPE_DOUBLE,
	NPARSER_OUTPUT_TYPE_INTEGER,
	NPARSER_OUTPUT_TYPE_DOUBLE,
	NPARSER_OUTPUT_TYPE_DOUBLE,
	NPARSER_OUTPUT_TYPE_DOUBLE,
	NPARSER_OUTPUT_TYPE_INTEGER,
	NPARSER_OUTPUT_TYPE_INTEGER,
	NPARSER_OUTPUT_TYPE_STRING
};
#endif // NDEVICEMFI_OUTLET_NMFIOUTLETDEVICESERVICE_INTERNE

#endif // !NDEVICEMFI_OUTLET_NMFIOUTLETDEVICESERVICE_PROTECT
