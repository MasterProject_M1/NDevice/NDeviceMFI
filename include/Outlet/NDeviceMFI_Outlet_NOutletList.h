#ifndef NDEVICEMFI_OUTLET_NOUTLETLIST_PROTECT
#define NDEVICEMFI_OUTLET_NOUTLETLIST_PROTECT

// --------------------------------------
// struct NDeviceMFI::Outlet::NOutletList
// --------------------------------------

typedef struct NOutletList
{
	// List
	NListe *m_list;
} NOutletList;

/**
 * Build outlet list
 *
 * @return the outlet list instance
 */
__ALLOC NOutletList *NDeviceMFI_Outlet_NOutletList_Build( void );

/**
 * Destroy outlet list instance
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletList_Destroy( NOutletList** );

/**
 * Update outlet list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The new incoming data
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceMFI_Outlet_NOutletList_Update( NOutletList*,
	const NParserOutputList *parserOutputList );

/**
 * Get outlet index from name
 *
 * @param this
 * 		This instance
 * @param name
 * 		The outlet name
 *
 * @return the outlet index or NERREUR
 */
__MUSTBEPROTECTED NU32 NDeviceMFI_Outlet_NOutletList_GetOutletIndexFromName( const NOutletList*,
	const char *name );

/**
 * Get outlet count
 *
 * @param this
 * 		This instance
 *
 * @return the outlet count
 */
__MUSTBEPROTECTED NU32 NDeviceMFI_Outlet_NOutletList_GetOutletCount( const NOutletList* );

/**
 * Get outlet
 *
 * @param this
 * 		This instance
 * @param index
 * 		The outlet index
 *
 * @return the outlet
 */
__MUSTBEPROTECTED const NOutlet *NDeviceMFI_Outlet_NOutletList_GetOutlet( const NOutletList*,
	NU32 index );

/**
 * Process outlet operation
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param index
 * 		The outlet index
 * @param isEnabled
 * 		Is outlet enabled?
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceMFI_Outlet_NOutletList_ProcessOutletOperation( NOutletList *this,
	NClientHTTP *httpClient,
	NU32 index,
	NBOOL isEnabled );

/**
 * Process outlet operation
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param name
 * 		The outlet name
 * @param isEnabled
 * 		Is outlet enabled?
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceMFI_Outlet_NOutletList_ProcessOutletOperation2( NOutletList*,
	NClientHTTP *httpClient,
	const char *name,
	NBOOL isEnabled );

/**
 * Activate protection
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletList_ActivateProtection( NOutletList* );

/**
 * Deactivate protection
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletList_DeactivateProtection( NOutletList* );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
__MUSTBEPROTECTED NBOOL NDeviceMFI_Outlet_NOutletList_BuildParserOutputList( NOutletList*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceMFI_Outlet_NOutletList_ProcessRESTGETRequest( NOutletList*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor );

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param httpClient
 * 		The http client
 * @param userData
 * 		The user data
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
__WILLLOCK __WILLUNLOCK NBOOL NDeviceMFI_Outlet_NOutletList_ProcessRESTPOSTRequest( NOutletList*,
	NClientHTTP *httpClient,
	const NParserOutputList *userData,
	const char *requestedElement,
	NU32 *cursor );

#endif // !NDEVICEMFI_OUTLET_NOUTLETLIST_PROTECT
