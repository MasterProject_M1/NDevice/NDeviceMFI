#ifndef NDEVICEMFI_OUTLET_NOUTLETPARSEDATA_PROTECT
#define NDEVICEMFI_OUTLET_NOUTLETPARSEDATA_PROTECT

// -------------------------------------------
// struct NDeviceMFI::Outlet::NOutletParseData
// -------------------------------------------

typedef struct NOutletParseData
{
	// Data
	NU32 m_portIndex;
	double m_power,
		m_energy,
		m_current,
		m_voltage,
		m_powerFactor;
	__CANBENULL char *m_id,
		*m_name,
		*m_model;
	NBOOL m_output,
		m_relay,
		m_lock;
} NOutletParseData;

/**
 * Destroy
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_Outlet_NOutletParseData_Free( NOutletParseData* );

/**
 * Read port data
 *
 * @param parserOutputList
 * 		The parser output list
 * @param cursor
 * 		The cursor in parser output list data
 * @param result
 * 		The result
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_Outlet_NOutletParseData_ReadPortData( const NParserOutputList *parserOutputList,
	NU32 *cursor,
	NOutletParseData *parseData );

#endif // !NDEVICEMFI_OUTLET_NOUTLETPARSEDATA_PROTECT
