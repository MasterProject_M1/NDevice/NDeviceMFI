#ifndef NDEVICEMFI_PROTECT
#define NDEVICEMFI_PROTECT

// --------------------
// namespace NDeviceMFI
// --------------------

// namespace NLib
#include "../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NHTTP
#include "../../../NLib/NHTTP/include/NHTTP.h"

// namespace NDeviceCommon
#include "../../NDeviceCommon/include/NDeviceCommon.h"

// namespace NJson
#include "../../../NParser/NJson/include/NJson.h"

// Default login to mFi devices
#define NDEVICE_MFI_DEFAULT_LOGIN_USERNAME				"ubnt"
#define NDEVICE_MFI_DEFAULT_LOGIN_PASSWORD				"ubnt"

#define NDEVICE_MFI_AIROS_SESSION_ID_CONSTANT			"AIROS_SESSIONID"
#define NDEVICE_MFI_DEFAULT_AIROS_SESSION_ID			"01234567890123456789012345678901"

#define NDEVICE_MFI_LOGIN_CONNECTION_TIMEOUT			1000
#define NDEVICE_MFI_DELAY_BETWEEN_UPDATE				8000
#define NDEVICE_MFI_RECEIVE_TIMEOUT						2200

#define NDEVICE_MFI_LOGIN_AUTHENTICATION_SENT_TIMEOUT	10000

// namespace NDeviceMFI::Outlet
#include "Outlet/NDeviceMFI_Outlet.h"

// struct NDeviceMFI::NDeviceMFI
#include "NDeviceMFI_NDeviceMFI.h"

// namespace NDeviceMFI::Service
#include "Service/NDeviceMFI_Service.h"

#endif // !NDEVICEMFI_PROTECT
