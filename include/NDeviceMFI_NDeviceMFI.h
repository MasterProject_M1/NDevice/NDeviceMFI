#ifndef NDEVICEMFI_NDEVICEMFI_PROTECT
#define NDEVICEMFI_NDEVICEMFI_PROTECT

// -----------------------------
// struct NDeviceMFI::NDeviceMFI
// -----------------------------

typedef struct NDeviceMFI
{
	// HTTP clients
	NClientHTTP *m_updateHTTPClient;
	NClientHTTP *m_outputHTTPClient;

	// Hostname
	char *m_hostname;

	// Port
	NU32 m_port;

	// Username
	char *m_username;

	// Password
	char *m_password;

	// Outlet list
	NOutletList *m_outletList;

	// Is authenticated
	NBOOL m_isAuthenticated;

	// Is authentication sent?
	NBOOL m_isAuthenticationSent;

	// Authentication sending time
	NU64 m_authenticationSendingTime;

	// Update thread
	NBOOL m_isUpdateThreadRunning;
	NThread *m_updateThread;
} NDeviceMFI;

/**
 * Build MFI device
 *
 * @param hostname
 * 		The hostname
 * @param port
 * 		The port
 *
 * @return the mfi built instance
 */
__ALLOC NDeviceMFI *NDeviceMFI_NDeviceMFI_Build( const char *hostname,
	NU32 port );

/**
 * Build MFI device
 *
 * @param hostname
 * 		The hostname
 * @param port
 * 		The port
 * @param username
 * 		The username
 * @param password
 * 		The password
 *
 * @return the mfi built instance
 */
__ALLOC NDeviceMFI *NDeviceMFI_NDeviceMFI_Build2( const char *hostname,
	NU32 port,
	const char *username,
	const char *password );

/**
 * Destroy mfi instance
 *
 * @param this
 * 		This instance
 */
void NDeviceMFI_NDeviceMFI_Destroy( NDeviceMFI** );

/**
 * Activate outlet
 *
 * @param this
 * 		This instance
 * @param index
 * 		The outlet index
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_ActivateOutlet( NDeviceMFI*,
	NU32 index );

/**
 * Deactivate outlet
 *
 * @param this
 * 		This instance
 * @param index
 * 		The outlet index
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_DeactivateOutlet( NDeviceMFI*,
	NU32 index );

/**
 * Build parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param keyRoot
 * 		The key root
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_BuildParserOutputList( NDeviceMFI *this,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *keyRoot );

/**
 * Process REST GET request
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The parser output list
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_ProcessRESTGETRequest( NDeviceMFI*,
	__OUTPUT NParserOutputList *parserOutputList,
	const char *requestedElement,
	NU32 *cursor );

/**
 * Process REST POST request
 *
 * @param this
 * 		This instance
 * @param userData
 * 		The user data
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 *
 * @return if the operation succeeded
 */
NBOOL NDeviceMFI_NDeviceMFI_ProcessRESTPOSTRequest( NDeviceMFI*,
	const NParserOutputList *userData,
	const char *requestedElement,
	NU32 *cursor );

/**
 * Get outlet list
 *
 * @param this
 * 		This instance
 *
 * @return the outlet list
 */
NOutletList *NDeviceMFI_NDeviceMFI_GetOutletList( NDeviceMFI* );

#endif // !NDEVICEMFI_NDEVICEMFI_PROTECT
